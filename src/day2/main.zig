const std = @import("std");

const Move = enum {
    rock,
    paper,
    scissors,
};

const Result = enum {
    win,
    draw,
    loss,
};

const EncryptionError = error{
    UnrecongizedInput,
};

fn decryptMove(input: u8) EncryptionError!Move {
    return switch (input) {
        // opponent move
        'A' => .rock,
        'B' => .paper,
        'C' => .scissors,

        // my move
        'X' => .rock,
        'Y' => .paper,
        'Z' => .scissors,

        else => EncryptionError.UnrecongizedInput,
    };
}

fn getHandResult(myMove: Move, opponentMove: Move) Result {
    const result: Result = switch (myMove) {
        .rock => switch (opponentMove) {
            .rock => Result.draw,
            .paper => Result.loss,
            .scissors => Result.win,
        },
        .paper => switch (opponentMove) {
            .rock => Result.win,
            .paper => Result.draw,
            .scissors => Result.loss,
        },
        .scissors => switch (opponentMove) {
            .rock => Result.loss,
            .paper => Result.win,
            .scissors => Result.draw,
        },
    };
    return result;
}

fn getMoveScore(move: Move) i32 {
    return switch (move) {
        .rock => 1,
        .paper => 2,
        .scissors => 3,
    };
}

fn getResultScore(result: Result) i32 {
    return switch (result) {
        .win => 6,
        .draw => 3,
        .loss => 0,
    };
}

fn getRoundScore(move: Move, result: Result) i32 {
    const moveScore: i32 = getMoveScore(move);
    const resultScore: i32 = getResultScore(result);
    return moveScore + resultScore;
}

/// Program Entry
pub fn main() !void {
    const fileName = "day2.txt";
    const file = try std.fs.cwd().openFile(fileName, .{});
    defer file.close();

    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    const allocator = arena.allocator();

    const read_buf = try file.readToEndAlloc(allocator, 1024 * 1024);
    defer allocator.free(read_buf);

    var totalScore: i32 = 0;

    var it = std.mem.tokenizeAny(u8, read_buf, "\n");
    while (it.next()) |item| {
        const opponentMove = try decryptMove(item[0]);
        const myMove = try decryptMove(item[2]);
        std.debug.print("{}: {}\n", .{ myMove, opponentMove });
        const result: Result = getHandResult(myMove, opponentMove);
        std.debug.print("Result for the round: {}\n", .{result});
        const score: i32 = getRoundScore(myMove, result);
        std.debug.print("Score for the round: {}\n", .{score});
        totalScore += score;
        std.debug.print("-------\n", .{});
    }

    std.debug.print("Final score: {}\n", .{totalScore});
}
